#include <iostream>

using namespace std;

int main() {
  const int length = 5;
  int table[length] = {1, 2, 3, 4, 5};
  int *pointer = table;      // begynnelsen av tabellen
  int *end = &table[length]; // adressen rett etter tabellslutt
  while (pointer < end) {    // sammenlikning av pekere
    cout << "Verdien til slutt er " << end << ", til pekeren er " << pointer << endl;
    cout << "Avstanden mellom pekeren og slutt er " << (end - pointer) << endl;
    pointer++;
  }
}
