#include <iostream>

using namespace std;

void swap(int &number_a, int &number_b) {
  int help = number_a;
  number_a = number_b;
  number_b = help;
}

int main() {
  int number1;
  int number2;
  cout << "Skriv to tall: ";
  cin >> number1 >> number2;
  if (number1 > number2) {
    swap(number1, number2);
  }
  cout << "I rekkefølge " << number1 << " " << number2 << endl;
}
